/**
 * Log is a final class which is used by other classes to print on the console.
 * Any other Logging methods will be added here. The project should not contain any
 * other logging apart from here. No System.out.printlns in the code.
 *
 * @author sanjitroy
 * @version %I%, %G%
 *
 */
public final class Log {

  //REFACTOR: due
  /**
   * Takes an array of Objects and prints it on a new line.
   * @param obj     array of objects
   */
  public static void p(Object ...obj) {
    for (Object o: obj) {
      System.out.println(o);
    }
  }


  //REFACTOR: due
  /**
   * Takes an array of objects and prints it on the same line.
   * @param obj     array of objects
   */
  public static void l(Object obj) {
    System.out.print(obj);
  }

}
