import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.ArrayList;

/**
 * Main class of Aponia, parses and lexes at the moment,
 * it would be changed to different classes.
 *
 * @author sanjitroy
 * @version %I%, %G%
 *
 */
public class Aponia {

  /**
   * Main method currently displays the tokens extracted from
   * an input stream.
   * It uses ANTLR4 to generate grammar from the grammar file Lisp.g4 which contains
   * the grammar of the LISP Programming language.
   * The
   *
   * @param args        Command line arguments
   */
  public static void main(String[] args) {
    Log.p("Loading: Aponia interpreter");
    String inp;

    while(true) {
      inp = Input.getINSTANCE().r();
      if (inp.equals("quit") || inp.equals("q") || inp.equals("exit")) {
        break;
      } else {
        parseInput(inp);
      }

    }
    //String inp = Input.getINSTANCE().r();

    //String code = "(+ 2/3*5-9 15)";

  }


  public static void parseInput(String input) {
    LispLexer lexer = new LispLexer(new ANTLRInputStream(input));
    CommonTokenStream tokenStream = new CommonTokenStream(lexer);

    LispParser parser = new LispParser(tokenStream);
    ParseTree tree = parser.program();


    System.out.println(tree.toStringTree(parser));

    tokenStream.fill();
    ArrayList<String> tokens = getTokens(tokenStream);

    for (String s: tokens) {
      Log.l(s + " ");
    }
    Log.p("");
  }

  /**
   * Generates an ArrayList containing all the token names from a given tokenStream.
   *
   * @param tokenStream               Contains a list of parsed tokens
   * @return ArrayList<String>        List of tokens got from tokenStream
   */
  public static ArrayList<String> getTokens(CommonTokenStream tokenStream) {
    ArrayList<String> tokens = new ArrayList<String>();

    for (Token t: tokenStream.getTokens()) {
      if (t.getType() == Token.EOF) {
        break;
      }

      tokens.add(LispLexer.ruleNames[t.getType()-1]);

    }
    return tokens;
  }

}
