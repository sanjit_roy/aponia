import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Input is a final class which is used by all other classes to take input
 * from the console. It would contain other ways as well like files.
 * This is a singleton.
 *
 * @author sanjitroy
 * @version %I%, %G%
 *
 */
public final class Input {
  private static Input INSTANCE;
  private BufferedReader buff;

  private Input() {
    buff = new BufferedReader(new InputStreamReader(System.in));
  }


  //REFACTOR: due
  /**
   * A static method which returns a INSTANCE of Input if one
   * is already present and if not present inititalizes a new one.
   *
   * @return Input    instance of Input class
   */
  public static Input getINSTANCE() {
    if (INSTANCE == null) {
      INSTANCE = new Input();
    }
    return INSTANCE;
  }


  //TODO: Throw a NullPointerException here
  //REFACTOR: due
  /**
   * Method which reads line from the console and sends as a string input.
   *
   * @return String   raw input line
   */
  public String r() {
    Log.l("aponia> ");
    String input = "";
    try {
      input = buff.readLine();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return input;
  }
}
